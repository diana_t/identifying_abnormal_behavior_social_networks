var express = require('express');
var router = express.Router();
var PageHandler = require('./pages.js');
var ApiHandler = require('./api.js');
var request = require('request');
var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

module.exports = function(db){
	var pageHandler = new PageHandler(db);
	var apiHandler = new ApiHandler(db);
	router.get('/', pageHandler.renderCollectorPage.bind(pageHandler));
	router.get('/test', pageHandler.renderTestPage.bind(pageHandler));
	
	router.post('/test', apiHandler.handleTest.bind(apiHandler));
	router.post('/collect', apiHandler.handleCollect.bind(apiHandler));
	router.get('/signout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	return router;
}

