var oauthObj = {
    'consumer_key' : 'z0YJjmP8avOfdqldS1ABu5mkK',
    'consumer_secret' : 'No4gA7zxLjmlNR4Yj4ne4zZrmdcncVAv33k5lZ2ueCeCiVakXe',
    'token' : '924323648676286464-4JwLTCpvgPUFnRUbP4RgFZOA88hkVp9',
    'token_secret' : 'G8d34FubVByNz97dcXn97fjSRH757EoKoQvOKCNoGpQwK'
};
var aop = require('aopromise');
var request = require('request');
var async = require('async');

class ApiHandler{
    constructor(db){
        this.db = db;
        var collector_factory = require('../src/collector.js');
        this.collector = collector_factory('posts', {db});
        var analyzer_factory = require('../src/analyzer.js');
        this.word_frequency_analyzer = analyzer_factory('word_frequency',{db})
        this.sentiment_analyzer = analyzer_factory('sentiment', {db});
        this.length_analyzer = analyzer_factory('length', {db});
        var tester_lib = require('../src/tester.js');
        this.tester = new tester_lib({db});
    }
    handleCollect(req, res, next){
        global.start = new Date();
        var self = this;
        self.collector.collect(req.body.userId, function(err, result){
            var finalResult = {};
            if(result.err){
                return res.send({"tweets" : []});
            }
            async.auto({
                "word_frequency" : function(word_frequency_cb){
                    self.word_frequency_analyzer.analyze(result, function(err, freqs){
                        finalResult["tweets"] = result;
                        finalResult["freqs"] = freqs.freqs;
                        finalResult["not_in_db_freqs"] = freqs.not_in_db_freqs;
                        word_frequency_cb();
                    });
                },
                "sentiment" : function(sentiment_cb){
                    self.sentiment_analyzer.analyze(result, function(err, sentiments){
                        finalResult["sentiment"] = sentiments.totalScore;
                        finalResult["not_in_db_sentiment"] = sentiments.notInDbScore;
                        sentiment_cb();
                    });
                },
                "length" : function(length_cb){
                    self.length_analyzer.analyze(result, function(err, lengths){
                        length_cb();
                    })
                }
            }, function(){
                res.send(finalResult);
            });
        });
        global.end = new Date();

        aop.register('logger', BeforeAfterTestsAspect);

        function changeUser(userId) {
            return global.start;
        }

        function createFctForBDstuff(userID){
            var user = userID;
            var fs = require('fs');
            self.collector.collect(req.body.userId, function(err, result){
                for(var idx = 0; idx < result.length; ++idx){
                    var insertInfoExpression = "INSERT INTO TWEETS(user_name, text) values ('" +  user + "', '" + result[idx].text+"');"+"\n";
                    fs.appendFile('queryForDB.txt', insertInfoExpression, (err) => {
                        if (err) throw err;
                    });
                }
                console.log('Insert saved!');
            })
            return global.start;
        }


        var loggedTime = aop()
            .logger('ChangeUserFctTime')
            .fn(changeUser);

        var infoForDB = aop()
        .logger('infoForBDstuff')
        .fn(createFctForBDstuff);


        loggedTime = aop(changeUser, new BeforeAfterTestsAspect('ChangeUserFctTime'));

        infoForDB = aop(createFctForBDstuff, new BeforeAfterTestsAspect('infoForBDstuff'));
         
        loggedTime(req.body.userId);
        infoForDB(req.body.userId);

        function BeforeAfterTestsAspect(funcName) {
            return new aop.AspectFrame(
                function (preOpts) {
                    console.log(funcName || preOpts.originalFunction.name, 'was called with', preOpts.args);
                },
                function (postOpts) {
                    console.log(funcName || postOpts.originalFunction.name, 'returned at', postOpts.result);
                }
            );
        }
    }
    handleTest(req, res, next){
        var userId = req.body.userId;
        var tweet = req.body.tweet;
        var self = this;

        self.tester.test(userId, tweet, function(err, test_result){
            var tweet_sentiment = test_result.tweet_sentiment;
            var db_sentiment = test_result.db_sentiment;
            var responseText = "Database sentiment score: ";
            responseText += (db_sentiment.score/db_sentiment.nr_words);
            responseText += " Current sentiment score: ";
            responseText += (tweet_sentiment.score/tweet_sentiment.tokens.length);
            responseText += " Average tweet word count: ";
            responseText += test_result.tweet_length.avgLength;
            responseText += " This tweet word count: ";
            responseText += test_result.tweet_length.thisLength;
            responseText += " Word frequency score: ";
            responseText += test_result.freqs.freqScore;
            return res.send(responseText);
        });
    }
}

module.exports = ApiHandler;