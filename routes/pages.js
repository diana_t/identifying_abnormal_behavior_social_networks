class PageHandler{
    constructor(db){
        this.db = db;
    }
    renderCollectorPage(req, res, next){
        res.render('collect');
    }
    renderTestPage(req, res, next){
        res.render('test_tweet');
    }
}

module.exports = PageHandler;