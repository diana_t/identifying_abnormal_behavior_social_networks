const test = require('tape')

global.start = new Date();

test('Trump has is named Trump', function (t)  {
    const { URL, URLSearchParams } = require('url');
    var url = new URL('https://api.twitter.com/1.1/statuses/show.json?screen_name=realDonaldTrump');
    const result = url.searchParams.get('screen_name');
    const expected = 'realDonaldTrump';
    t.ok(result);
    t.deepEqual(result, expected);
    t.end();
});


test('This is Trump ids', function (t) {
    const { URL, URLSearchParams } = require('url');
    var url = new URL('https://api.twitter.com/1.1/statuses/show.json?id=927715648477724672');
    const result = url.searchParams.get('id');
    const expected = '927715648477724672';
    t.ok(result);
    t.equal(result, expected);
    t.end();    
});

test('timing test', function (t) {
    t.plan(2);
    
    t.equal(typeof Date.now, 'function');
    var start = Date.now();
    
    setTimeout(function () {
        t.notEqual(Date.now() - start, 100);
    }, 100);
});

global.end = new Date();

var AOPGen = function() {
  return {
    logBefore: function(fn, logMsg) {
      return function() {
        console.log(logMsg);
        return fn.apply(this, arguments);
      };
    },

    logAfter: function(fn, logMsg) {
      return function() {
        var result = fn.apply(this.arguments);
          console.log(logMsg);
          return result;
      };
    },

    measureTime: function(fn) {
      return function() {
        var startt = new Date();
        var result = fn.apply(this, arguments);
        var endd = new Date();
        var diff = endd - startt;
        console.log("Execution time: " + (diff/1000) + "s");
      };
    }
  }
}();

function calcTime(start, end) {
    var diff = end - start;
    return diff
}
calcTime=AOPGen.logBefore(calcTime, 'inainte')
calcTime=AOPGen.logBefore(calcTime, 'dupa')
calcTime=AOPGen.measureTime(calcTime);

console.log(calcTime(start, end))