# Identifying Abnormal Behavior in Social Networks

## Team members

* Andrei Răzvan Măzăreanu 
* Catalina Prisacaru 
* Diana Turnea 

## Team coordination
 Adrian Iftene - Associate Professor, PhD

## From Faculty of Computer Science Iasi

## Links
* [UML](https://drive.google.com/open?id=0B0XnhrocLpuuczlJVzZ4enNSMVU)
* [State-of-the-art](https://docs.google.com/document/d/1LKG_xG5oXDH2AStXmwFYqQgcx3zVR3KJvgnxRWjIoeM/edit?usp=sharing)
* [Requirements Analysis Document](https://docs.google.com/document/d/1HsTA__SdMu2hzqRsQGR2VzivX0J4nsJsVLJS9gCuDrc/edit?usp=sharing)
* [Team resources and discussion](https://docs.google.com/spreadsheets/d/1GmWqWamXgGBm-YK_89_GQ5Vc2iPNImcd8X80Qc_7k7o/edit?usp=sharing)
* [Technical Report](https://docs.google.com/document/d/1zt0tL8Nyp3Ky7-f_MGh7NdBcE_oDT5XgJNo1m7wwl-k/edit)