const test = require('tape')
var aop = require('aopromise');


global.start = new Date();

test('Trump has is named Trump', function (t)  {
    const { URL, URLSearchParams } = require('url');
    var url = new URL('https://api.twitter.com/1.1/statuses/show.json?screen_name=realDonaldTrump');
    const result = url.searchParams.get('screen_name');
    const expected = 'realDonaldTrump';
    t.ok(result);
    t.deepEqual(result, expected);
    t.end();
});


test('This is Trump ids', function (t) {
    const { URL, URLSearchParams } = require('url');
    var url = new URL('https://api.twitter.com/1.1/statuses/show.json?id=927715648477724672');
    const result = url.searchParams.get('id');
    const expected = '927715648477724672';
    t.ok(result);
    t.equal(result, expected);
    t.end();    
});

test('timing test', function (t) {
    t.plan(2);
    
    t.equal(typeof Date.now, 'function');
    var start = Date.now();
    
    setTimeout(function () {
        t.notEqual(Date.now() - start, 100);
    }, 100);
});

global.end = new Date();


aop.register('logger', BeforeAfterTestsAspect);

function timming(start, end) {
  var diff =((end-start)/1000);
    console.log("Execution time: " + diff + "s");
    return diff;
}


var loggedTime = aop()
    .logger('TimmingFct')
    .fn(timming);


 loggedTime = aop(timming, new BeforeAfterTestsAspect('TimmingFct'));
 
loggedTime(global.start, global.end);

function BeforeAfterTestsAspect(funcName) {
    return new aop.AspectFrame(
        function (preOpts) {
            console.log(funcName || preOpts.originalFunction.name, 'was called with', preOpts.args);
        },
        function (postOpts) {
            console.log(funcName || postOpts.originalFunction.name, 'returned with', postOpts.result);
        }
    );
}
