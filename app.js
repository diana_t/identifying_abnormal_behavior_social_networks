var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var MongoClient = require('mongodb').MongoClient;

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', '.ejs');

app.use(favicon());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var expressSession = require('express-session');
app.use(expressSession({secret: 'mySecretKey'}));

MongoClient.connect("mongodb://localhost:27017/social_networks", function(err, db){
    console.info("Mam conectat");
    if(err){throw err};
    var routes = require('./routes/index')(db);
    app.use('/', routes);
    console.info("Listening");
    app.listen(3000);    
})



