'use strict';

var Promise = require('bluebird');
var aopromise = require('../');
var aop = aopromise.wrap;
var AspectPack = aopromise.AspectPack;
var BenchmarkAspect = require('./aspects/BenchmarkAspect');
var MemoizeAspect = require('./aspects/MemoizeAspect');
var NodeRSA = require('node-rsa');
var key = new NodeRSA({b: 512});

var fs = require('fs');
global.paramsFromFile = fs.readFileSync('../params.txt','utf8');

function myFunc(text) {
  var encrypted = key.encrypt(text, 'base64');
  console.log('encrypted: ', encrypted);
  console.log('myFunc body executed ' + text);
  return Promise.resolve(encrypted);
}
var benchmarkedAndMemoizedMyFunc = aop(myFunc, new BenchmarkAspect(), new MemoizeAspect());

Promise.resolve().then(function (paramsFromFile) {
  console.log('--- BENCHMARKED AND MEMOIZED ---');
  console.log('--- second execution should be faster   ---');

  return benchmarkedAndMemoizedMyFunc(global.paramsFromFile).then(console.log)
    .then(function () {
      return benchmarkedAndMemoizedMyFunc(global.paramsFromFile).then(console.log)
    });
});