(function(){
	var app = angular.module('userCollector',[]);

	// app.config(function($locationProvider){
	// 	$locationProvider.html5Mode({"enabled" : true,"requireBase" : false});
	// })
	
	app.controller('TestController', ['$scope', '$http', '$location', function($scope, $http, $location){
		var self = this;

        this.loading = false;
        this.user = "";
        this.tweet = "";
        this.done = false;

        this.send = function(){
            self.loading = true;
            $http({
                "method" : "POST",
                "data" : {"userId" : self.user, "tweet" : self.tweet},
                "url" : "/test"
            })
            .success(function(d,h,s,c){
                console.info(d);
                self.response = d;
                self.done = true;
                self.loading = false;
            })
            .error(function(){
                alert("Error");
            });
        }

	}]);
})();