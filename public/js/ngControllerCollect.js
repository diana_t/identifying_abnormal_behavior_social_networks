(function(){
	var app = angular.module('userCollector',[]);

	// app.config(function($locationProvider){
	// 	$locationProvider.html5Mode({"enabled" : true,"requireBase" : false});
	// })
	
	app.controller('CollectController', ['$scope', '$http', '$location', function($scope, $http, $location){
		var self = this;

        this.loading = false;
        this.user = "";
        this.done = false;

        this.collect = function(){
            self.loading = true;
            $http({
                "method" : "POST",
                "data" : {"userId" : self.user},
                "url" : "/collect"
            })
            .success(function(d,h,s,c){
                self.tweets = d.tweets;
                self.freqs = d.freqs;
                var sortable = [];
                for(key in self.freqs){
                    sortable.push([key, self.freqs[key]]);
                }
                sortable.sort(function(a,b){
                    return b[1] - a[1]
                })
                self.words = sortable;
                self.done = true;
                console.info(d);
                self.loading = false;
            })
            .error(function(){
                alert("Error");
            });
        }

	}]);
})();