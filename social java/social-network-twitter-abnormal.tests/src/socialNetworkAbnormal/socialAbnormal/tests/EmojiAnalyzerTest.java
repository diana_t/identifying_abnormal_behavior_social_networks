/**
 */
package socialNetworkAbnormal.socialAbnormal.tests;

import junit.textui.TestRunner;

import socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer;
import socialNetworkAbnormal.socialAbnormal.SocialAbnormalFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Emoji Analyzer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EmojiAnalyzerTest extends AnalyzerTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EmojiAnalyzerTest.class);
	}

	/**
	 * Constructs a new Emoji Analyzer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmojiAnalyzerTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Emoji Analyzer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EmojiAnalyzer getFixture() {
		return (EmojiAnalyzer)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SocialAbnormalFactory.eINSTANCE.createEmojiAnalyzer());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EmojiAnalyzerTest
