/**
 */
package socialNetworkAbnormal.socialAbnormal.tests;

import junit.textui.TestRunner;

import socialNetworkAbnormal.socialAbnormal.EmotionExtractor;
import socialNetworkAbnormal.socialAbnormal.SocialAbnormalFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Emotion Extractor</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EmotionExtractorTest extends AnalyzerTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EmotionExtractorTest.class);
	}

	/**
	 * Constructs a new Emotion Extractor test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmotionExtractorTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Emotion Extractor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EmotionExtractor getFixture() {
		return (EmotionExtractor)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SocialAbnormalFactory.eINSTANCE.createEmotionExtractor());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EmotionExtractorTest
