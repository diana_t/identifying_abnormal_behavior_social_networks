/**
 */
package socialNetworkAbnormal.socialAbnormal.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import socialNetworkAbnormal.socialAbnormal.DAO;
import socialNetworkAbnormal.socialAbnormal.SocialAbnormalFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>DAO</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DAOTest extends TestCase {

	/**
	 * The fixture for this DAO test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAO fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DAOTest.class);
	}

	/**
	 * Constructs a new DAO test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAOTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this DAO test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DAO fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this DAO test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAO getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SocialAbnormalFactory.eINSTANCE.createDAO());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DAOTest
