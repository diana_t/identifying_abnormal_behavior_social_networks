/**
 */
package socialAbnormal.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import socialAbnormal.Analyzer;
import socialAbnormal.SocialAbnormalFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Analyzer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class AnalyzerTest extends TestCase {

	/**
	 * The fixture for this Analyzer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Analyzer fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(AnalyzerTest.class);
	}

	/**
	 * Constructs a new Analyzer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalyzerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Analyzer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Analyzer fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Analyzer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Analyzer getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SocialAbnormalFactory.eINSTANCE.createAnalyzer());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //AnalyzerTest
