/**
 */
package socialNetworkAbnormal.socialAbnormal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Analyzer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link socialNetworkAbnormal.socialAbnormal.Analyzer#getUser <em>User</em>}</li>
 * </ul>
 *
 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getAnalyzer()
 * @model
 * @generated
 */
public interface Analyzer extends EObject {
	/**
	 * Returns the value of the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User</em>' reference.
	 * @see #setUser(User)
	 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getAnalyzer_User()
	 * @model
	 * @generated
	 */
	User getUser();

	/**
	 * Sets the value of the '{@link socialNetworkAbnormal.socialAbnormal.Analyzer#getUser <em>User</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User</em>' reference.
	 * @see #getUser()
	 * @generated
	 */
	void setUser(User value);

} // Analyzer
