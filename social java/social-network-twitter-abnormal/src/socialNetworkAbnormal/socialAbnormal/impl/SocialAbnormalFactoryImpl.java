/**
 */
package socialNetworkAbnormal.socialAbnormal.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import socialNetworkAbnormal.socialAbnormal.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SocialAbnormalFactoryImpl extends EFactoryImpl implements SocialAbnormalFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SocialAbnormalFactory init() {
		try {
			SocialAbnormalFactory theSocialAbnormalFactory = (SocialAbnormalFactory)EPackage.Registry.INSTANCE.getEFactory(SocialAbnormalPackage.eNS_URI);
			if (theSocialAbnormalFactory != null) {
				return theSocialAbnormalFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SocialAbnormalFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SocialAbnormalFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SocialAbnormalPackage.USER: return createUser();
			case SocialAbnormalPackage.EMOTION_EXTRACTOR: return createEmotionExtractor();
			case SocialAbnormalPackage.EMOJI_ANALYZER: return createEmojiAnalyzer();
			case SocialAbnormalPackage.ANALYZER: return createAnalyzer();
			case SocialAbnormalPackage.DAO: return createDAO();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public User createUser() {
		UserImpl user = new UserImpl();
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmotionExtractor createEmotionExtractor() {
		EmotionExtractorImpl emotionExtractor = new EmotionExtractorImpl();
		return emotionExtractor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmojiAnalyzer createEmojiAnalyzer() {
		EmojiAnalyzerImpl emojiAnalyzer = new EmojiAnalyzerImpl();
		return emojiAnalyzer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Analyzer createAnalyzer() {
		AnalyzerImpl analyzer = new AnalyzerImpl();
		return analyzer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAO createDAO() {
		DAOImpl dao = new DAOImpl();
		return dao;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SocialAbnormalPackage getSocialAbnormalPackage() {
		return (SocialAbnormalPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SocialAbnormalPackage getPackage() {
		return SocialAbnormalPackage.eINSTANCE;
	}

} //SocialAbnormalFactoryImpl
