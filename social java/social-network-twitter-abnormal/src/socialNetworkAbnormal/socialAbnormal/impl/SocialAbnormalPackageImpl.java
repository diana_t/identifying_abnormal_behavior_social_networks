/**
 */
package socialNetworkAbnormal.socialAbnormal.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import socialNetworkAbnormal.socialAbnormal.Analyzer;
import socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer;
import socialNetworkAbnormal.socialAbnormal.EmotionExtractor;
import socialNetworkAbnormal.socialAbnormal.SocialAbnormalFactory;
import socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage;
import socialNetworkAbnormal.socialAbnormal.User;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SocialAbnormalPackageImpl extends EPackageImpl implements SocialAbnormalPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emotionExtractorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emojiAnalyzerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass analyzerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass daoEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SocialAbnormalPackageImpl() {
		super(eNS_URI, SocialAbnormalFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SocialAbnormalPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SocialAbnormalPackage init() {
		if (isInited) return (SocialAbnormalPackage)EPackage.Registry.INSTANCE.getEPackage(SocialAbnormalPackage.eNS_URI);

		// Obtain or create and register package
		SocialAbnormalPackageImpl theSocialAbnormalPackage = (SocialAbnormalPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SocialAbnormalPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SocialAbnormalPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theSocialAbnormalPackage.createPackageContents();

		// Initialize created meta-data
		theSocialAbnormalPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSocialAbnormalPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SocialAbnormalPackage.eNS_URI, theSocialAbnormalPackage);
		return theSocialAbnormalPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUser() {
		return userEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUser_Metrics() {
		return (EAttribute)userEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEmotionExtractor() {
		return emotionExtractorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEmotionExtractor_Normal_metrics() {
		return (EAttribute)emotionExtractorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEmotionExtractor_DbManager() {
		return (EAttribute)emotionExtractorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEmotionExtractor_Option() {
		return (EAttribute)emotionExtractorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEmotionExtractor_Dao() {
		return (EReference)emotionExtractorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEmojiAnalyzer() {
		return emojiAnalyzerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEmojiAnalyzer_Normal_metrics() {
		return (EAttribute)emojiAnalyzerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEmojiAnalyzer_DbManager() {
		return (EAttribute)emojiAnalyzerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEmojiAnalyzer_Option() {
		return (EAttribute)emojiAnalyzerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEmojiAnalyzer_Dao() {
		return (EReference)emojiAnalyzerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnalyzer() {
		return analyzerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnalyzer_User() {
		return (EReference)analyzerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAO() {
		return daoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAO_DbConnection() {
		return (EAttribute)daoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SocialAbnormalFactory getSocialAbnormalFactory() {
		return (SocialAbnormalFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		userEClass = createEClass(USER);
		createEAttribute(userEClass, USER__METRICS);

		emotionExtractorEClass = createEClass(EMOTION_EXTRACTOR);
		createEAttribute(emotionExtractorEClass, EMOTION_EXTRACTOR__NORMAL_METRICS);
		createEAttribute(emotionExtractorEClass, EMOTION_EXTRACTOR__DB_MANAGER);
		createEAttribute(emotionExtractorEClass, EMOTION_EXTRACTOR__OPTION);
		createEReference(emotionExtractorEClass, EMOTION_EXTRACTOR__DAO);

		emojiAnalyzerEClass = createEClass(EMOJI_ANALYZER);
		createEAttribute(emojiAnalyzerEClass, EMOJI_ANALYZER__NORMAL_METRICS);
		createEAttribute(emojiAnalyzerEClass, EMOJI_ANALYZER__DB_MANAGER);
		createEAttribute(emojiAnalyzerEClass, EMOJI_ANALYZER__OPTION);
		createEReference(emojiAnalyzerEClass, EMOJI_ANALYZER__DAO);

		analyzerEClass = createEClass(ANALYZER);
		createEReference(analyzerEClass, ANALYZER__USER);

		daoEClass = createEClass(DAO);
		createEAttribute(daoEClass, DAO__DB_CONNECTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		emotionExtractorEClass.getESuperTypes().add(this.getAnalyzer());
		emojiAnalyzerEClass.getESuperTypes().add(this.getAnalyzer());

		// Initialize classes, features, and operations; add parameters
		initEClass(userEClass, User.class, "User", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUser_Metrics(), ecorePackage.getEString(), "metrics", null, 0, 1, User.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(emotionExtractorEClass, EmotionExtractor.class, "EmotionExtractor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEmotionExtractor_Normal_metrics(), ecorePackage.getEString(), "normal_metrics", null, 0, 1, EmotionExtractor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEmotionExtractor_DbManager(), ecorePackage.getEString(), "dbManager", null, 0, 1, EmotionExtractor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEmotionExtractor_Option(), ecorePackage.getEString(), "option", null, 0, 1, EmotionExtractor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEmotionExtractor_Dao(), this.getDAO(), null, "dao", null, 0, -1, EmotionExtractor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(emojiAnalyzerEClass, EmojiAnalyzer.class, "EmojiAnalyzer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEmojiAnalyzer_Normal_metrics(), ecorePackage.getEString(), "normal_metrics", null, 0, 1, EmojiAnalyzer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEmojiAnalyzer_DbManager(), ecorePackage.getEString(), "dbManager", null, 0, 1, EmojiAnalyzer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEmojiAnalyzer_Option(), ecorePackage.getEString(), "option", null, 0, 1, EmojiAnalyzer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEmojiAnalyzer_Dao(), this.getDAO(), null, "dao", null, 0, -1, EmojiAnalyzer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(analyzerEClass, Analyzer.class, "Analyzer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnalyzer_User(), this.getUser(), null, "user", null, 0, 1, Analyzer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(daoEClass, socialNetworkAbnormal.socialAbnormal.DAO.class, "DAO", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAO_DbConnection(), ecorePackage.getEString(), "dbConnection", null, 0, 1, socialNetworkAbnormal.socialAbnormal.DAO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //SocialAbnormalPackageImpl
