/**
 */
package socialNetworkAbnormal.socialAbnormal;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage
 * @generated
 */
public interface SocialAbnormalFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SocialAbnormalFactory eINSTANCE = socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>User</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User</em>'.
	 * @generated
	 */
	User createUser();

	/**
	 * Returns a new object of class '<em>Emotion Extractor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Emotion Extractor</em>'.
	 * @generated
	 */
	EmotionExtractor createEmotionExtractor();

	/**
	 * Returns a new object of class '<em>Emoji Analyzer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Emoji Analyzer</em>'.
	 * @generated
	 */
	EmojiAnalyzer createEmojiAnalyzer();

	/**
	 * Returns a new object of class '<em>Analyzer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Analyzer</em>'.
	 * @generated
	 */
	Analyzer createAnalyzer();

	/**
	 * Returns a new object of class '<em>DAO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAO</em>'.
	 * @generated
	 */
	DAO createDAO();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SocialAbnormalPackage getSocialAbnormalPackage();

} //SocialAbnormalFactory
