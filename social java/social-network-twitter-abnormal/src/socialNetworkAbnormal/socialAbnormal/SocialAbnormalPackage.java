/**
 */
package socialNetworkAbnormal.socialAbnormal;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalFactory
 * @model kind="package"
 * @generated
 */
public interface SocialAbnormalPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "socialAbnormal";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/socialAbnormal";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "socialAbnormal";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SocialAbnormalPackage eINSTANCE = socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl.init();

	/**
	 * The meta object id for the '{@link socialNetworkAbnormal.socialAbnormal.impl.UserImpl <em>User</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see socialNetworkAbnormal.socialAbnormal.impl.UserImpl
	 * @see socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl#getUser()
	 * @generated
	 */
	int USER = 0;

	/**
	 * The feature id for the '<em><b>Metrics</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__METRICS = 0;

	/**
	 * The number of structural features of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link socialNetworkAbnormal.socialAbnormal.impl.AnalyzerImpl <em>Analyzer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see socialNetworkAbnormal.socialAbnormal.impl.AnalyzerImpl
	 * @see socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl#getAnalyzer()
	 * @generated
	 */
	int ANALYZER = 3;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYZER__USER = 0;

	/**
	 * The number of structural features of the '<em>Analyzer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYZER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Analyzer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYZER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link socialNetworkAbnormal.socialAbnormal.impl.EmotionExtractorImpl <em>Emotion Extractor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see socialNetworkAbnormal.socialAbnormal.impl.EmotionExtractorImpl
	 * @see socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl#getEmotionExtractor()
	 * @generated
	 */
	int EMOTION_EXTRACTOR = 1;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOTION_EXTRACTOR__USER = ANALYZER__USER;

	/**
	 * The feature id for the '<em><b>Normal metrics</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOTION_EXTRACTOR__NORMAL_METRICS = ANALYZER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Db Manager</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOTION_EXTRACTOR__DB_MANAGER = ANALYZER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Option</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOTION_EXTRACTOR__OPTION = ANALYZER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Dao</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOTION_EXTRACTOR__DAO = ANALYZER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Emotion Extractor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOTION_EXTRACTOR_FEATURE_COUNT = ANALYZER_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Emotion Extractor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOTION_EXTRACTOR_OPERATION_COUNT = ANALYZER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link socialNetworkAbnormal.socialAbnormal.impl.EmojiAnalyzerImpl <em>Emoji Analyzer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see socialNetworkAbnormal.socialAbnormal.impl.EmojiAnalyzerImpl
	 * @see socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl#getEmojiAnalyzer()
	 * @generated
	 */
	int EMOJI_ANALYZER = 2;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOJI_ANALYZER__USER = ANALYZER__USER;

	/**
	 * The feature id for the '<em><b>Normal metrics</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOJI_ANALYZER__NORMAL_METRICS = ANALYZER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Db Manager</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOJI_ANALYZER__DB_MANAGER = ANALYZER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Option</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOJI_ANALYZER__OPTION = ANALYZER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Dao</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOJI_ANALYZER__DAO = ANALYZER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Emoji Analyzer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOJI_ANALYZER_FEATURE_COUNT = ANALYZER_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Emoji Analyzer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMOJI_ANALYZER_OPERATION_COUNT = ANALYZER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link socialNetworkAbnormal.socialAbnormal.impl.DAOImpl <em>DAO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see socialNetworkAbnormal.socialAbnormal.impl.DAOImpl
	 * @see socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl#getDAO()
	 * @generated
	 */
	int DAO = 4;

	/**
	 * The feature id for the '<em><b>Db Connection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAO__DB_CONNECTION = 0;

	/**
	 * The number of structural features of the '<em>DAO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAO_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>DAO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAO_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link socialNetworkAbnormal.socialAbnormal.User <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.User
	 * @generated
	 */
	EClass getUser();

	/**
	 * Returns the meta object for the attribute '{@link socialNetworkAbnormal.socialAbnormal.User#getMetrics <em>Metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metrics</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.User#getMetrics()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Metrics();

	/**
	 * Returns the meta object for class '{@link socialNetworkAbnormal.socialAbnormal.EmotionExtractor <em>Emotion Extractor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Emotion Extractor</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.EmotionExtractor
	 * @generated
	 */
	EClass getEmotionExtractor();

	/**
	 * Returns the meta object for the attribute '{@link socialNetworkAbnormal.socialAbnormal.EmotionExtractor#getNormal_metrics <em>Normal metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Normal metrics</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.EmotionExtractor#getNormal_metrics()
	 * @see #getEmotionExtractor()
	 * @generated
	 */
	EAttribute getEmotionExtractor_Normal_metrics();

	/**
	 * Returns the meta object for the attribute '{@link socialNetworkAbnormal.socialAbnormal.EmotionExtractor#getDbManager <em>Db Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Db Manager</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.EmotionExtractor#getDbManager()
	 * @see #getEmotionExtractor()
	 * @generated
	 */
	EAttribute getEmotionExtractor_DbManager();

	/**
	 * Returns the meta object for the attribute '{@link socialNetworkAbnormal.socialAbnormal.EmotionExtractor#getOption <em>Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Option</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.EmotionExtractor#getOption()
	 * @see #getEmotionExtractor()
	 * @generated
	 */
	EAttribute getEmotionExtractor_Option();

	/**
	 * Returns the meta object for the containment reference list '{@link socialNetworkAbnormal.socialAbnormal.EmotionExtractor#getDao <em>Dao</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dao</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.EmotionExtractor#getDao()
	 * @see #getEmotionExtractor()
	 * @generated
	 */
	EReference getEmotionExtractor_Dao();

	/**
	 * Returns the meta object for class '{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer <em>Emoji Analyzer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Emoji Analyzer</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer
	 * @generated
	 */
	EClass getEmojiAnalyzer();

	/**
	 * Returns the meta object for the attribute '{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getNormal_metrics <em>Normal metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Normal metrics</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getNormal_metrics()
	 * @see #getEmojiAnalyzer()
	 * @generated
	 */
	EAttribute getEmojiAnalyzer_Normal_metrics();

	/**
	 * Returns the meta object for the attribute '{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getDbManager <em>Db Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Db Manager</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getDbManager()
	 * @see #getEmojiAnalyzer()
	 * @generated
	 */
	EAttribute getEmojiAnalyzer_DbManager();

	/**
	 * Returns the meta object for the attribute '{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getOption <em>Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Option</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getOption()
	 * @see #getEmojiAnalyzer()
	 * @generated
	 */
	EAttribute getEmojiAnalyzer_Option();

	/**
	 * Returns the meta object for the containment reference list '{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getDao <em>Dao</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dao</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getDao()
	 * @see #getEmojiAnalyzer()
	 * @generated
	 */
	EReference getEmojiAnalyzer_Dao();

	/**
	 * Returns the meta object for class '{@link socialNetworkAbnormal.socialAbnormal.Analyzer <em>Analyzer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Analyzer</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.Analyzer
	 * @generated
	 */
	EClass getAnalyzer();

	/**
	 * Returns the meta object for the reference '{@link socialNetworkAbnormal.socialAbnormal.Analyzer#getUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>User</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.Analyzer#getUser()
	 * @see #getAnalyzer()
	 * @generated
	 */
	EReference getAnalyzer_User();

	/**
	 * Returns the meta object for class '{@link socialNetworkAbnormal.socialAbnormal.DAO <em>DAO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAO</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.DAO
	 * @generated
	 */
	EClass getDAO();

	/**
	 * Returns the meta object for the attribute '{@link socialNetworkAbnormal.socialAbnormal.DAO#getDbConnection <em>Db Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Db Connection</em>'.
	 * @see socialNetworkAbnormal.socialAbnormal.DAO#getDbConnection()
	 * @see #getDAO()
	 * @generated
	 */
	EAttribute getDAO_DbConnection();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SocialAbnormalFactory getSocialAbnormalFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link socialNetworkAbnormal.socialAbnormal.impl.UserImpl <em>User</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see socialNetworkAbnormal.socialAbnormal.impl.UserImpl
		 * @see socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl#getUser()
		 * @generated
		 */
		EClass USER = eINSTANCE.getUser();

		/**
		 * The meta object literal for the '<em><b>Metrics</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__METRICS = eINSTANCE.getUser_Metrics();

		/**
		 * The meta object literal for the '{@link socialNetworkAbnormal.socialAbnormal.impl.EmotionExtractorImpl <em>Emotion Extractor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see socialNetworkAbnormal.socialAbnormal.impl.EmotionExtractorImpl
		 * @see socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl#getEmotionExtractor()
		 * @generated
		 */
		EClass EMOTION_EXTRACTOR = eINSTANCE.getEmotionExtractor();

		/**
		 * The meta object literal for the '<em><b>Normal metrics</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMOTION_EXTRACTOR__NORMAL_METRICS = eINSTANCE.getEmotionExtractor_Normal_metrics();

		/**
		 * The meta object literal for the '<em><b>Db Manager</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMOTION_EXTRACTOR__DB_MANAGER = eINSTANCE.getEmotionExtractor_DbManager();

		/**
		 * The meta object literal for the '<em><b>Option</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMOTION_EXTRACTOR__OPTION = eINSTANCE.getEmotionExtractor_Option();

		/**
		 * The meta object literal for the '<em><b>Dao</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMOTION_EXTRACTOR__DAO = eINSTANCE.getEmotionExtractor_Dao();

		/**
		 * The meta object literal for the '{@link socialNetworkAbnormal.socialAbnormal.impl.EmojiAnalyzerImpl <em>Emoji Analyzer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see socialNetworkAbnormal.socialAbnormal.impl.EmojiAnalyzerImpl
		 * @see socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl#getEmojiAnalyzer()
		 * @generated
		 */
		EClass EMOJI_ANALYZER = eINSTANCE.getEmojiAnalyzer();

		/**
		 * The meta object literal for the '<em><b>Normal metrics</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMOJI_ANALYZER__NORMAL_METRICS = eINSTANCE.getEmojiAnalyzer_Normal_metrics();

		/**
		 * The meta object literal for the '<em><b>Db Manager</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMOJI_ANALYZER__DB_MANAGER = eINSTANCE.getEmojiAnalyzer_DbManager();

		/**
		 * The meta object literal for the '<em><b>Option</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMOJI_ANALYZER__OPTION = eINSTANCE.getEmojiAnalyzer_Option();

		/**
		 * The meta object literal for the '<em><b>Dao</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMOJI_ANALYZER__DAO = eINSTANCE.getEmojiAnalyzer_Dao();

		/**
		 * The meta object literal for the '{@link socialNetworkAbnormal.socialAbnormal.impl.AnalyzerImpl <em>Analyzer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see socialNetworkAbnormal.socialAbnormal.impl.AnalyzerImpl
		 * @see socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl#getAnalyzer()
		 * @generated
		 */
		EClass ANALYZER = eINSTANCE.getAnalyzer();

		/**
		 * The meta object literal for the '<em><b>User</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANALYZER__USER = eINSTANCE.getAnalyzer_User();

		/**
		 * The meta object literal for the '{@link socialNetworkAbnormal.socialAbnormal.impl.DAOImpl <em>DAO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see socialNetworkAbnormal.socialAbnormal.impl.DAOImpl
		 * @see socialNetworkAbnormal.socialAbnormal.impl.SocialAbnormalPackageImpl#getDAO()
		 * @generated
		 */
		EClass DAO = eINSTANCE.getDAO();

		/**
		 * The meta object literal for the '<em><b>Db Connection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAO__DB_CONNECTION = eINSTANCE.getDAO_DbConnection();

	}

} //SocialAbnormalPackage
