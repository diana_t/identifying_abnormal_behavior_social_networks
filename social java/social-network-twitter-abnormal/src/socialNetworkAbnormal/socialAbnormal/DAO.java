/**
 */
package socialNetworkAbnormal.socialAbnormal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link socialNetworkAbnormal.socialAbnormal.DAO#getDbConnection <em>Db Connection</em>}</li>
 * </ul>
 *
 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getDAO()
 * @model
 * @generated
 */
public interface DAO extends EObject {
	/**
	 * Returns the value of the '<em><b>Db Connection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Db Connection</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Db Connection</em>' attribute.
	 * @see #setDbConnection(String)
	 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getDAO_DbConnection()
	 * @model
	 * @generated
	 */
	String getDbConnection();

	/**
	 * Sets the value of the '{@link socialNetworkAbnormal.socialAbnormal.DAO#getDbConnection <em>Db Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Db Connection</em>' attribute.
	 * @see #getDbConnection()
	 * @generated
	 */
	void setDbConnection(String value);

} // DAO
