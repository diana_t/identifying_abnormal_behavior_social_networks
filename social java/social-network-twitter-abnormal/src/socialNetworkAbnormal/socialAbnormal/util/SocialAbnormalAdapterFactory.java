/**
 */
package socialNetworkAbnormal.socialAbnormal.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import socialNetworkAbnormal.socialAbnormal.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage
 * @generated
 */
public class SocialAbnormalAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SocialAbnormalPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SocialAbnormalAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SocialAbnormalPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SocialAbnormalSwitch<Adapter> modelSwitch =
		new SocialAbnormalSwitch<Adapter>() {
			@Override
			public Adapter caseUser(User object) {
				return createUserAdapter();
			}
			@Override
			public Adapter caseEmotionExtractor(EmotionExtractor object) {
				return createEmotionExtractorAdapter();
			}
			@Override
			public Adapter caseEmojiAnalyzer(EmojiAnalyzer object) {
				return createEmojiAnalyzerAdapter();
			}
			@Override
			public Adapter caseAnalyzer(Analyzer object) {
				return createAnalyzerAdapter();
			}
			@Override
			public Adapter caseDAO(DAO object) {
				return createDAOAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link socialNetworkAbnormal.socialAbnormal.User <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see socialNetworkAbnormal.socialAbnormal.User
	 * @generated
	 */
	public Adapter createUserAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link socialNetworkAbnormal.socialAbnormal.EmotionExtractor <em>Emotion Extractor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see socialNetworkAbnormal.socialAbnormal.EmotionExtractor
	 * @generated
	 */
	public Adapter createEmotionExtractorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer <em>Emoji Analyzer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer
	 * @generated
	 */
	public Adapter createEmojiAnalyzerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link socialNetworkAbnormal.socialAbnormal.Analyzer <em>Analyzer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see socialNetworkAbnormal.socialAbnormal.Analyzer
	 * @generated
	 */
	public Adapter createAnalyzerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link socialNetworkAbnormal.socialAbnormal.DAO <em>DAO</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see socialNetworkAbnormal.socialAbnormal.DAO
	 * @generated
	 */
	public Adapter createDAOAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SocialAbnormalAdapterFactory
