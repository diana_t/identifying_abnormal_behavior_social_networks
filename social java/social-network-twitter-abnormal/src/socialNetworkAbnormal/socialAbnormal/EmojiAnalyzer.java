/**
 */
package socialNetworkAbnormal.socialAbnormal;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Emoji Analyzer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getNormal_metrics <em>Normal metrics</em>}</li>
 *   <li>{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getDbManager <em>Db Manager</em>}</li>
 *   <li>{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getOption <em>Option</em>}</li>
 *   <li>{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getDao <em>Dao</em>}</li>
 * </ul>
 *
 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getEmojiAnalyzer()
 * @model
 * @generated
 */
public interface EmojiAnalyzer extends Analyzer {
	/**
	 * Returns the value of the '<em><b>Normal metrics</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normal metrics</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normal metrics</em>' attribute.
	 * @see #setNormal_metrics(String)
	 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getEmojiAnalyzer_Normal_metrics()
	 * @model
	 * @generated
	 */
	String getNormal_metrics();

	/**
	 * Sets the value of the '{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getNormal_metrics <em>Normal metrics</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normal metrics</em>' attribute.
	 * @see #getNormal_metrics()
	 * @generated
	 */
	void setNormal_metrics(String value);

	/**
	 * Returns the value of the '<em><b>Db Manager</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Db Manager</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Db Manager</em>' attribute.
	 * @see #setDbManager(String)
	 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getEmojiAnalyzer_DbManager()
	 * @model
	 * @generated
	 */
	String getDbManager();

	/**
	 * Sets the value of the '{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getDbManager <em>Db Manager</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Db Manager</em>' attribute.
	 * @see #getDbManager()
	 * @generated
	 */
	void setDbManager(String value);

	/**
	 * Returns the value of the '<em><b>Option</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Option</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Option</em>' attribute.
	 * @see #setOption(String)
	 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getEmojiAnalyzer_Option()
	 * @model
	 * @generated
	 */
	String getOption();

	/**
	 * Sets the value of the '{@link socialNetworkAbnormal.socialAbnormal.EmojiAnalyzer#getOption <em>Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Option</em>' attribute.
	 * @see #getOption()
	 * @generated
	 */
	void setOption(String value);

	/**
	 * Returns the value of the '<em><b>Dao</b></em>' containment reference list.
	 * The list contents are of type {@link socialNetworkAbnormal.socialAbnormal.DAO}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dao</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dao</em>' containment reference list.
	 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getEmojiAnalyzer_Dao()
	 * @model containment="true"
	 * @generated
	 */
	EList<DAO> getDao();

} // EmojiAnalyzer
