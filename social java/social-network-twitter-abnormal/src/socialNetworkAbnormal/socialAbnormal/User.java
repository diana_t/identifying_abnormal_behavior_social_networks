/**
 */
package socialNetworkAbnormal.socialAbnormal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link socialNetworkAbnormal.socialAbnormal.User#getMetrics <em>Metrics</em>}</li>
 * </ul>
 *
 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getUser()
 * @model
 * @generated
 */
public interface User extends EObject {
	/**
	 * Returns the value of the '<em><b>Metrics</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metrics</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metrics</em>' attribute.
	 * @see #setMetrics(String)
	 * @see socialNetworkAbnormal.socialAbnormal.SocialAbnormalPackage#getUser_Metrics()
	 * @model
	 * @generated
	 */
	String getMetrics();

	/**
	 * Sets the value of the '{@link socialNetworkAbnormal.socialAbnormal.User#getMetrics <em>Metrics</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metrics</em>' attribute.
	 * @see #getMetrics()
	 * @generated
	 */
	void setMetrics(String value);

} // User
