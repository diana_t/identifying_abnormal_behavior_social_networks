/**
 */
package socialAbnormal.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import socialAbnormal.DAO;
import socialAbnormal.SocialAbnormalPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link socialAbnormal.impl.DAOImpl#getDbConnection <em>Db Connection</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DAOImpl extends MinimalEObjectImpl.Container implements DAO {
	/**
	 * The default value of the '{@link #getDbConnection() <em>Db Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDbConnection()
	 * @generated
	 * @ordered
	 */
	protected static final String DB_CONNECTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDbConnection() <em>Db Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDbConnection()
	 * @generated
	 * @ordered
	 */
	protected String dbConnection = DB_CONNECTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SocialAbnormalPackage.Literals.DAO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDbConnection() {
		return dbConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDbConnection(String newDbConnection) {
		String oldDbConnection = dbConnection;
		dbConnection = newDbConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SocialAbnormalPackage.DAO__DB_CONNECTION, oldDbConnection, dbConnection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SocialAbnormalPackage.DAO__DB_CONNECTION:
				return getDbConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SocialAbnormalPackage.DAO__DB_CONNECTION:
				setDbConnection((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SocialAbnormalPackage.DAO__DB_CONNECTION:
				setDbConnection(DB_CONNECTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SocialAbnormalPackage.DAO__DB_CONNECTION:
				return DB_CONNECTION_EDEFAULT == null ? dbConnection != null : !DB_CONNECTION_EDEFAULT.equals(dbConnection);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dbConnection: ");
		result.append(dbConnection);
		result.append(')');
		return result.toString();
	}

} //DAOImpl
