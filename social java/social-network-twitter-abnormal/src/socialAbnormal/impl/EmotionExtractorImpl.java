/**
 */
package socialAbnormal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import socialAbnormal.DAO;
import socialAbnormal.EmotionExtractor;
import socialAbnormal.SocialAbnormalPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Emotion Extractor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link socialAbnormal.impl.EmotionExtractorImpl#getNormal_metrics <em>Normal metrics</em>}</li>
 *   <li>{@link socialAbnormal.impl.EmotionExtractorImpl#getDbManager <em>Db Manager</em>}</li>
 *   <li>{@link socialAbnormal.impl.EmotionExtractorImpl#getOption <em>Option</em>}</li>
 *   <li>{@link socialAbnormal.impl.EmotionExtractorImpl#getDao <em>Dao</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EmotionExtractorImpl extends AnalyzerImpl implements EmotionExtractor {
	/**
	 * The default value of the '{@link #getNormal_metrics() <em>Normal metrics</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormal_metrics()
	 * @generated
	 * @ordered
	 */
	protected static final String NORMAL_METRICS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNormal_metrics() <em>Normal metrics</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormal_metrics()
	 * @generated
	 * @ordered
	 */
	protected String normal_metrics = NORMAL_METRICS_EDEFAULT;

	/**
	 * The default value of the '{@link #getDbManager() <em>Db Manager</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDbManager()
	 * @generated
	 * @ordered
	 */
	protected static final String DB_MANAGER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDbManager() <em>Db Manager</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDbManager()
	 * @generated
	 * @ordered
	 */
	protected String dbManager = DB_MANAGER_EDEFAULT;

	/**
	 * The default value of the '{@link #getOption() <em>Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOption()
	 * @generated
	 * @ordered
	 */
	protected static final String OPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOption() <em>Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOption()
	 * @generated
	 * @ordered
	 */
	protected String option = OPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDao() <em>Dao</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDao()
	 * @generated
	 * @ordered
	 */
	protected EList<DAO> dao;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EmotionExtractorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SocialAbnormalPackage.Literals.EMOTION_EXTRACTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNormal_metrics() {
		return normal_metrics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNormal_metrics(String newNormal_metrics) {
		String oldNormal_metrics = normal_metrics;
		normal_metrics = newNormal_metrics;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SocialAbnormalPackage.EMOTION_EXTRACTOR__NORMAL_METRICS, oldNormal_metrics, normal_metrics));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDbManager() {
		return dbManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDbManager(String newDbManager) {
		String oldDbManager = dbManager;
		dbManager = newDbManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SocialAbnormalPackage.EMOTION_EXTRACTOR__DB_MANAGER, oldDbManager, dbManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOption() {
		return option;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOption(String newOption) {
		String oldOption = option;
		option = newOption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SocialAbnormalPackage.EMOTION_EXTRACTOR__OPTION, oldOption, option));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAO> getDao() {
		if (dao == null) {
			dao = new EObjectContainmentEList<DAO>(DAO.class, this, SocialAbnormalPackage.EMOTION_EXTRACTOR__DAO);
		}
		return dao;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__DAO:
				return ((InternalEList<?>)getDao()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__NORMAL_METRICS:
				return getNormal_metrics();
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__DB_MANAGER:
				return getDbManager();
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__OPTION:
				return getOption();
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__DAO:
				return getDao();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__NORMAL_METRICS:
				setNormal_metrics((String)newValue);
				return;
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__DB_MANAGER:
				setDbManager((String)newValue);
				return;
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__OPTION:
				setOption((String)newValue);
				return;
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__DAO:
				getDao().clear();
				getDao().addAll((Collection<? extends DAO>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__NORMAL_METRICS:
				setNormal_metrics(NORMAL_METRICS_EDEFAULT);
				return;
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__DB_MANAGER:
				setDbManager(DB_MANAGER_EDEFAULT);
				return;
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__OPTION:
				setOption(OPTION_EDEFAULT);
				return;
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__DAO:
				getDao().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__NORMAL_METRICS:
				return NORMAL_METRICS_EDEFAULT == null ? normal_metrics != null : !NORMAL_METRICS_EDEFAULT.equals(normal_metrics);
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__DB_MANAGER:
				return DB_MANAGER_EDEFAULT == null ? dbManager != null : !DB_MANAGER_EDEFAULT.equals(dbManager);
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__OPTION:
				return OPTION_EDEFAULT == null ? option != null : !OPTION_EDEFAULT.equals(option);
			case SocialAbnormalPackage.EMOTION_EXTRACTOR__DAO:
				return dao != null && !dao.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (normal_metrics: ");
		result.append(normal_metrics);
		result.append(", dbManager: ");
		result.append(dbManager);
		result.append(", option: ");
		result.append(option);
		result.append(')');
		return result.toString();
	}

} //EmotionExtractorImpl
