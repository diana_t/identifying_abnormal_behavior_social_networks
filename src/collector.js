var PostsCollector = require('./posts_collector.js');
/**
 * Factory for the data collectors
 * @param type Type of the analyser. Can be one of ['posts']
 * @param env Environment
 * @callback callback Has two parameters: error and return value. Can be omitted
 * @returns The created object or null
 */
module.exports = function(type, env, callback){
	switch (type){
		case "posts": {
			var retVal = PostsCollector.getInstance(env);
			if(typeof callback === 'function'){
				return callback(null, retVal);
			}
			return retVal;
		}
		default : {
			if(typeof callback === 'function'){
				return callback(null, null);
			}
			return null;
		}
	}
}