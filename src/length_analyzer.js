/**
 * Implements the Singleton design pattern.
 */
var count = require('word-count');
var async = require('async');
class SentimentAnalyzer{
	static getInstance(env){
		if (!this.instance){
			this.instance = new SentimentAnalyzer(env);
		};
		return this.instance;
	}
	constructor(env){
		this.env = env;
	}
	train(old_inputs){
		
	}
	analyze(new_input, callback){
        var nrNewPosts = 0;
        var nrNewWords = 0;
        var self = this;
        async.each(new_input, function(doc, cb){
            try{
                var nrWords = count(doc.text);
            } catch(e){
                var nrWords = 0;
            }
            
            self.env.db.collection('tweets').findOne({"_id" : doc._id}, function(err, db_doc){
                if(!db_doc || !db_doc.processed_by_length){
                    nrNewPosts++;
                    nrNewWords += nrWords;
                    self.env.db.collection('tweets').update({"_id" : doc._id}, {"$set" : {"processed_by_length" : true}}, cb);
                } else {
                    cb();
                }
            });
        }, function(){
            self.env.db.collection('length').update(
                {"_id" : new_input[0].user.screen_name}, 
                {"$inc" : {"nr_posts" : nrNewPosts, "nr_words" : nrNewWords}}, {"upsert" : true}, 
                function(){
                    callback(null, {nrNewPosts, nrNewWords});
                }
            );
        });
	}
}

SentimentAnalyzer.intance = null;

module.exports = SentimentAnalyzer;