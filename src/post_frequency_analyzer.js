/**
 * Implements the Singleton design pattern.
 */

class PostFrequencyAnalyser{
	static getInstance(env){
		if (!this.instance){
			this.instance = new PostFrequencyAnalyser(env);
		};
		return this.instance;
	}
	constructor(env){
		this.env = env;
	}
	train(old_inputs){
		
	}
	analyze(new_input){

	}
}

PostFrequencyAnalyser.intance = null;

module.exports = PostFrequencyAnalyser;