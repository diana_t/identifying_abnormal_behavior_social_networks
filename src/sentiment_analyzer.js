/**
 * Implements the Singleton design pattern.
 * Sunt folosite bazele acestea de date:
 * http://www2.imm.dtu.dk/pubdb/views/publication_details.php?id=6010
 * http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0144296
 */
var sentiment = require('sentiment');
var count = require('word-count');
var async = require('async');
class SentimentAnalyzer{
	static getInstance(env){
		if (!this.instance){
			this.instance = new SentimentAnalyzer(env);
		};
		return this.instance;
	}
	constructor(env){
		this.env = env;
	}
	train(old_inputs){
		
	}
	analyze(new_input, callback){
        var totalScore = 0;
        var totalComparative = 0;
        var notInDbScore = 0;
        var notInDbComparative = 0;
        var word_nr = 0;
        var not_in_db_word_nr = 0;
        var self = this;
        if(new_input.errors){
            callback(null, {});
        }
        async.each(new_input, function(doc, cb){
            var extractedSentiment = sentiment(doc.text);
            totalScore += extractedSentiment.score;
            totalComparative += extractedSentiment.comparative;
            word_nr += extractedSentiment.words.length; //Numarul de cuvinte recunoscute in db.
            self.env.db.collection('tweets').findOne({"_id" : doc._id}, function(err, db_doc){
                if(db_doc && !db_doc.processed_by_sentiment){
                    notInDbScore += extractedSentiment.score;
                    notInDbComparative += extractedSentiment.comparative;
                    not_in_db_word_nr += extractedSentiment.words.length;
                    self.env.db.collection('tweets').update({"_id" : doc._id}, {"$set" : {"processed_by_sentiment" : true}}, cb);
                } else {
                    cb();
                }
            });
        }, function(){
            self.env.db.collection('sentiment').update(
                {"_id" : new_input[0].user.screen_name}, 
                {"$inc" : {"score" : notInDbScore, "comparative" : notInDbComparative, "nr_words" : not_in_db_word_nr}}, {"upsert" : true}, 
                function(){
                    callback(null, {totalScore, totalComparative, notInDbScore, notInDbComparative, word_nr, not_in_db_word_nr});
                }
            );
        });
	}
}

SentimentAnalyzer.intance = null;

module.exports = SentimentAnalyzer;