var EmojiAnalyzer = require('./emoji_analyzer.js');
var PostFrequencyAnalyser = require('./post_frequency_analyzer.js');
var WordFrequencyAnalyzer = require('./word_frequency_analyzer.js');
var SentimentAnalyzer = require('./sentiment_analyzer.js');
var LengthAnalyzer = require('./length_analyzer.js');

module.exports = function(type, env, callback){
	switch (type){
		case "sentiment" : {
			var retVal = SentimentAnalyzer.getInstance(env);
			if(typeof callback === 'function'){
				return callback(null, retVal);
			}
			return retVal;
		}
		case "word_frequency" : {
			var retVal = WordFrequencyAnalyzer.getInstance(env);
			if(typeof callback === 'function'){
				return callback(null, retVal);
			}
			return retVal;
		}
		case "emoji": {
			var retVal = EmojiAnalyzer.getInstance(env);
			if(typeof callback === 'function'){
				return callback(null, retVal);
			}
			return retVal;
		}
		case 'post_frequency' : {
			var retVal = PostFrequencyAnalyser.getInstance(env);
			if(typeof callback === 'function'){
				return callback(null, retVal);
			}
			return retVal;
		}
		case 'length' : {
			var retVal = LengthAnalyzer.getInstance(env);
			if(typeof callback === 'function'){
				return callback(null, retVal);
			}
			return retVal;
		}
		default : {
			if(typeof callback === 'function'){
				return callback(null, null);
			}
			return null;
		}
	}
}