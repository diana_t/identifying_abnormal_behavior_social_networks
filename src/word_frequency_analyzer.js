/**
 * Implements the Singleton design pattern.
 */
var natural = require('natural');
natural.PorterStemmer.attach();
var async = require('async');
class WordFrequencyAnalyzer{
	static getInstance(env){
		if (!this.instance){
			this.instance = new WordFrequencyAnalyzer(env);
		};
		return this.instance;
	}
	constructor(env){
		this.env = env;
	}
	train(old_inputs){
		
	}
	analyze(new_input, callback){
        var freqs = {};
        var not_in_db_freqs = {};
        var self = this;
        async.each(new_input, function(doc, cb){
            if(!doc.text){
                return cb();
            }
            var tokens = doc.text.tokenizeAndStem();
            self.env.db.collection('tweets').findOne({"_id" : doc._id}, function(err, db_doc){

                for(var idx = 0; idx < tokens.length; ++idx){
                    freqs[tokens[idx]] = 1 + (freqs[tokens[idx]] || 0);
                    if(db_doc && !db_doc.processed_by_word_frequency){
                        not_in_db_freqs[tokens[idx]] = 1 + (not_in_db_freqs[tokens[idx]] || 0);
                    }
                }
                if(db_doc && !db_doc.processed_by_word_frequency){
                    self.env.db.collection('tweets').update({"_id" : doc._id}, {"$set" : {"processed_by_word_frequency" : true}}, cb)
                } else {
                    cb();
                }
            });
        }, function(){
            var updateDoc = {"$inc" : {}};
            var keys = Object.keys(not_in_db_freqs);
            for(var key of keys){
                updateDoc["$inc"]["freqs." + key] = not_in_db_freqs[key];
            }
            if(!new_input[0] || !new_input[0].user || !new_input[0].user.screen_name){
                return callback(null, {"freqs" : {}, "not_in_db_freqs" : {}});
            }
            self.env.db.collection('freqs').update({"_id" : new_input[0].user.screen_name}, updateDoc, {"upsert" : true}, function(){
                callback(null, {freqs, not_in_db_freqs});
            })
        });
	}
}

WordFrequencyAnalyzer.intance = null;

module.exports = WordFrequencyAnalyzer;