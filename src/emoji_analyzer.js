/**
 * Implements the Singleton design pattern.
 */

class EmojiAnalyzer{
	static getInstance(env){
		if (!this.instance){
			this.instance = new EmojiAnalyzer(env);
		};
		return this.instance;
	}
	constructor(env){
		this.env = env;
	}
	train(old_inputs){
		
	}
	analyze(new_input){

	}
}

EmojiAnalyzer.intance = null;

module.exports = EmojiAnalyzer;