/**
 * Implements the Singleton design pattern.
 */
var oauthObj = {
    'consumer_key' : 'z0YJjmP8avOfdqldS1ABu5mkK',
    'consumer_secret' : 'No4gA7zxLjmlNR4Yj4ne4zZrmdcncVAv33k5lZ2ueCeCiVakXe',
    'token' : '924323648676286464-4JwLTCpvgPUFnRUbP4RgFZOA88hkVp9',
    'token_secret' : 'G8d34FubVByNz97dcXn97fjSRH757EoKoQvOKCNoGpQwK'
};
var request = require('request');
var async = require('async');
class PostsCollector{
	static getInstance(env){
		if (!this.instance){
			this.instance = new PostsCollector(env);
		};
		return this.instance;
	}
	constructor(env){
		this.env = env;
	}
	collect(userId, big_callback){
		var url = 'https://api.twitter.com/1.1/statuses/user_timeline.json?count=200&include_rts=false&screen_name=' + userId; 
		//var url = 'https://api.twitter.com/1.1/application/rate_limit_status.json';
		var self = this;
		request.get({'url' : url, oauth: oauthObj}, function(result, headers, body){
			body = JSON.parse(body);
			console.info("Am primit", body.length);
			if(body.errors){
				return big_callback(null, {"err" : true});
			}
			async.each(body, function(doc, cb){

				var tweet_url = 'https://api.twitter.com/1.1/statuses/show/' + doc.id + '.json';
				request.get({'url' : tweet_url, oauth: oauthObj}, function(result, headers, _body){
					if(typeof _body === 'string'){
						_body = JSON.parse(_body);
					}

					var inserted_doc = doc;
					if(!_body.errors){
						inserted_doc = _body;
					}
					inserted_doc['_id'] = inserted_doc['id'];
					delete inserted_doc['id'];
					doc = inserted_doc;
					self.env.db.collection('tweets').insert(inserted_doc, function(err, result){
						if(err && err.code !== 11000){
							return cb(err);
						}
						return cb();
					});
				})
			}, function(){
				big_callback(null, body);
			});
		});
	}
}

PostsCollector.intance = null;

module.exports = PostsCollector;