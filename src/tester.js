var async = require('async');
var sentiment = require('sentiment');
var natural = require('natural');
natural.PorterStemmer.attach();
var count = require('word-count');

class Tester{
    constructor(env){
        this.env = env;
    }
    test(userId, tweet, callback){
        var self = this;
        async.auto({
            "db_sentiment" : function(_cb){
                self.env.db.collection('sentiment').findOne({"_id" : userId}, _cb)
            },
            "tweet_sentiment" : function(_cb){
                _cb(null, sentiment(tweet));
            },
            "tweet_length" : function(_cb){
                self.env.db.collection('length').findOne({"_id" : userId}, function(err, db_doc){
                    var thisLength = count(tweet);
                    var avgLength = db_doc.nr_words/db_doc.nr_posts;
                    _cb(null, {avgLength, thisLength});
                });
            },
            "freqs" : ["tweet_sentiment", function(results, _cb){
                self.env.db.collection('freqs').findOne({"_id" : userId}, function(err, db_doc){
                    var db_freqs = db_doc.freqs;
                    var db_freqs_sorted = Object.keys(db_freqs).sort(function(a,b){return db_freqs[b] - db_freqs[a]});
                    var tokens = tweet.tokenizeAndStem();
                    var freqs = {};
                    for(var idx = 0; idx < tokens.length; ++idx){
                        freqs[tokens[idx]] = 1 + (freqs[tokens[idx]] || 0);
                    }
                    var freqs_sorted = Object.keys(freqs).sort(function(a,b){return freqs[b] - freqs[a]});
                    var freqScore = 0;
                    for(var idx = 0; idx < freqs_sorted.length; ++idx){
                        var word = freqs_sorted[idx];
                        var db_idx = db_freqs_sorted.indexOf(word);
                        //Daca cuvantul a mai fost folosit si e in prima jumatate ca cuvinte folosite.
                        //Cu cat idx e mai mare, inseamna ca cuvantul ar trebui sa aduca un scor mai mic. Cu cat db_idx e mai mic, cu atat scorul trb sa fie mai mare.
                        //Deci o sa adunam db_idx/idx
                        if(db_idx > 0 && db_idx <= db_freqs_sorted.length / 2){
                            freqScore += db_idx/(idx+1);
                        }
                    }
                    _cb(null,{freqScore});
                });
            }]
        },function(err, results){
            //Facem si un scor total.
            // if(Math.abs(results.db_sentiment.score/results.db_sentiment.nr_words) - (results.tweet_sentiment.score/results.tweet_sentiment.tokens.length) > 3){
            //     results.suspect_sentiment = true;
            // }
            return callback(null, results);
        });
    }
}

module.exports = Tester;